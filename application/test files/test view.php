profile: <br/>

name : <?= $playerData["name"] ?> <br/>
email : <?= $playerData["email"] ?> <br/>
description : <?= $playerData["desc"] ?> <br/>
location : <?= $playerData["loc"] ?> <br/> <br/>


history : <br/>

    <li data-role="list-divider">Games Played</li>
    <? foreach ($playedGames as $game): ?>
       <li>
            <?= $game["name"] ?> <br/>
            <?= $game["startDate"] ?> <br/>
            <?= $game["endDate"] ?> <br/>
            <?= $game["place"] ?>
        </li>
    <? endforeach ?>
    <li data-role="list-divider">Games Administered</li>
    <? foreach ($adminedGames as $game): ?>
        <li>
            <?= $game["name"] ?> <br/>
            <?= $game["startDate"] ?> <br/>
            <?= $game["endDate"] ?> <br/>
            <?= $game["place"] ?>
        </li>
    <? endforeach ?>
    
kills :
<? foreach ($kills as $kill): ?>
    <? // if player mode ?>
        killer : <?= $kill["killerID"] ?> <br/>
    <? // endif ?>
    
    victim : <?= $kill["victimID"] ?> <br/>
    time : <?= $kill["time"] ?> <br/>
    
<? endforeach ?> <br/><br/>

target : <br/>

name : <?= $targetData["name"] ?> <br/>
email : <?= $targetData["email"] ?> <br/>
description : <?= $targetData["desc"] ?> <br/>
location : <?= $targetData["loc"] ?> <br/> <br/>

notifications : <br/>

<? foreach ($notifications as $notification) : ?>
    content : <?= $notification["content"] ?>, 
    time : <?= $notification["time"] ?> <br/>
<? endforeach ?>

players (for admins) <br/>

<? foreach ($players as $pair) : ?>
    <? // if $ptair["target"] == NULL then player is dead ?>
    
    player : <?= $pair["player"] ?> , 
    target : <?= $pari["target"] ?> <br/>
<? endforeach ?>

