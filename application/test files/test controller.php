<?
/*****************************************************************************
 * user_evan_test.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Controller file. Calls user_model.php model.
 * Includes methods for sending test data to the view via the renderPage 
 * private method.
 ****************************************************************************/

class User_evan_test extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("user_model");
    }
    
    public function login() {
        $meta["title"] = "Assassins Mobile";
        $meta["methodName"] = "login";
        $meta["mode"] = "none";
        $data["errorMessage"] = "Error Message Placeholder";
        
        $this->renderPage($meta, $data);
    }
    
    public function join() {
        $meta["title"] = "Join a Game";
        $meta["methodName"] = "join";
        $meta["mode"] = "none";
        $data["errorMessage"] = "Error Message Placeholder";
        
        $this->renderPage($meta, $data);
    }
    
    public function create() {
        $meta["title"] = "Create a Game";
        $meta["methodName"] = "create";
        $meta["mode"] = "none";
        $data["errorMessage"] = "Error Message Placeholder";
        
        $this->renderPage($meta, $data);
    }
    
    public function home() {
        $meta["title"] = "Home";
        $meta["methodName"] = "home";
        $meta["mode"] = "none";
        $data = array("playerGames" =>
                    array(
                        array("gameID" => 1, "name" => "Eliot Game"),
                        array("gameID" => 2, "name" => "Kirkland Game (Incest)")
                    ),
                    "adminGames" =>
                    array(
                        array("gameID" => 3, "name" => "Mather Shit"),
                        array("gameID" => 4, "name" => "Winthrop Is The Best")
                    )
                );
        
        $this->renderPage($meta, $data);
    }
    
     public function rules() {
        $meta["title"] = "Game Name";
        $meta["methodName"] = "rules";
        $meta["mode"] = "admin";
        $data = array("gameKillingRules" => "Kill by hitting with wad of paper.", 
                      "gameSafetyZones" => "No killing in classes or in dining halls",
                      "gameAdditionalRules" => "No alliances.",
                      "mode" => "admin");
        
        $this->renderPage($meta, $data);
    }
    
    public function editRules() {
        $meta["title"] = "Game Name";
        $meta["methodName"] = "editRules";
        $meta["mode"] = "admin";
        $data = array("gameKillingRules" => "Kill by hitting with wad of paper.", 
                      "gameSafetyZones" => "No killing in classes or in dining halls",
                      "gameAdditionalRules" => "No alliances.",
                      "errorMessage" => "Error Message Placeholder");
        
        $this->renderPage($meta, $data);
    }
    
    
    
    private function renderPage($meta, $data) {
        // this method is responsible for ultimately rendering views
        $this->load->view("templates/header", $meta);
        $this->load->view("assassins/" . $meta["methodName"], $data);
        $this->load->view("templates/footer", $meta);           
    }
}

?>
