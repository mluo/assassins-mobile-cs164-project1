<?
/*****************************************************************************
 * user_model.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Model file. Called by game.php and user.php controllers.
 * Includes methods for retrieving controller-requested data from SQL 
 * databases. Methods generally prepare SQL query statements based on 
 * controller input and then return the query results back to the controller.
 ****************************************************************************/

// login, create, logout adapted from Simplelogin.php (http://codeigniter.com/wiki/Simplelogin)

class User_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    /*
	 * Retrieves current or past games user is playing or administering
	 *
	 * @access	public
	 * @param	bool
	 * @return	array
	 */
    public function getGames($playing = true, $history = false) {
        // check current user id
        $userID = $this->session->userdata("id");
    
        // get inforation about games
        $this->db->select("*");
        $this->db->from("games");
        
        // if playing the game, the game and player IDs will be in the player table
        if ($playing) {
            $this->db->join("players", "games.gameID = players.gameID");
            $this->db->where("players.playerID", $userID);
        }
        
        // if administering the game, the game and admin IDs will be in the admin table
        else {
            $this->db->where("games.adminID", $userID);  
        }
        
        // if game is in history, the end date has been set
        if ($history) {
            $this->db->where("games.endDate is not NULL");
            $this->db->order_by("games.endDate", "desc");
        }
        
        // if the game is still in progress, the end date is null
        else {
            $this->db->where("games.endDate is NULL");
            $this->db->order_by("games.name", "asc");
        }
        
        $query = $this->db->get();
        $results = $query->result_array();
        
        $games = array();
        
        // retrieve winners of past games (for administered games history)
        if ($history && !$playing) {
			foreach ($results as $game) {
				$winner = $this->getPlayerName($this->getWinner($game["gameID"]));
				
				$games[] = array("name" => $game["name"],
								 "gameID" => $game["gameID"],
								 "winner" => $winner,
								 "startDate" => $game["startDate"],
								 "endDate" => $game["endDate"] );
			}
			
			return $games;
        }
        
        return $results;
    }

    /*
	 * Retrieves name of a user, based on the user ID
	 *
	 * @access	private
	 * @param	int
	 * @return	string
	 */    
    private function getPlayerName($userID) {
    	$this->db->select("name");
    	$this->db->where("userID", $userID);
    	$query = $this->db->get("users");
    	$result = $query->row_array();
    	
    	// only return name if it's a valid ID number
    	if (isset($result["name"]))
	    	return $result["name"];
	    
	    return "";
    }
    
    /*
	 * Retrieves profile information about user.
	 *
	 * @access	public
	 * @param	int
	 * @return	array
	 */
    public function getProfile($playerID) {
        $this->db->select("name, email, desc, loc");
        $this->db->where("userID", $playerID);
        
        $query = $this->db->get("users");
        
        return $query->row_array();
    }
    
    /*
	 * Retrieves user's current target and target's proifle information.
	 *
	 * @access	public
	 * @return	array
	 */
    public function getTarget() {
        $this->db->select("targetID");
        $this->db->where("playerID", $this->session->userdata("id"));
        $this->db->where("gameID", $this->session->userdata("game"));
        
        $query = $this->db->get("players");
        
        if ($query->num_rows() < 1)
            return array("name" => "Something went wrong...");
        
        $result = $query->row_array();
        
        $target = $this->getProfile($result["targetID"]);
        
        return $target;
    }

    /*
	 * Retrieves winner of a game, based on the game ID.
	 *
	 * @access	private
	 * @param	ing
	 * @return	string
	 */    
    private function getWinner($gameID) {
    	$this->db->select("playerID");
    	$this->db->where("gameID", $gameID);
    	$this->db->where("place", 1);
    	$query = $this->db->get("players");
    	$result = $query->row_array();
    	
    	// only return result if game ID is valid
    	if (isset($result["playerID"]))
	    	return $result["playerID"];
	    	
	    return "";
    }
    
    /*
	 * Creates a new user and inserts it into database and returns error message or "no error"
	 *
	 * @access	public
	 * @param	array
	 * @param	bool
	 * @return	string
	 */
    function create($params, $auto_login = true) {
		// Make sure account info was sent
		if($params['email'] == "" || $params['password'] == "") {
			return "Email or password is blank.";
		}
		
		// Check against user table
		$this->db->where('email', $params['email']); 
		$query = $this->db->get("users");
		
		if ($query->num_rows() > 0) {
			// email already registered
			return "Email is already registered";
			
		} else {
			// Encrypt password
			$params['password'] = md5($params['password']);

			$this->db->set($params); 
			if(!$this->db->insert("users")) {
				// There was a problem!
				return "Something went wrong... Please try another email or password.";						
			}
			$user_id = $this->db->insert_id();
			
			// Automatically login to created account
			if($auto_login) {		
				// Destroy old session
				$this->session->sess_destroy();
				
				// Create a fresh, brand new session
				$this->session->sess_create();
				
				// Set session data
				$this->session->set_userdata(array('id' => $user_id,'email' => $params['email']));
				
				// Set logged_in to true
				$this->session->set_userdata(array('logged_in' => true));			
			
			}
			
			// Login was successful			
			return "no error";
		}
	}
	
	/*
	 * Logs in user and returns error message or "no error"
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param   bool
	 * @return	string
	 */
    public function login($email, $password, $auto_login = true) {    
		// Make sure login info was sent
		if($email == '' OR $password == '') {
			return "Password or email is missing";
		}

		// Check if already logged in
		if($this->session->userdata('email') == $email) {
			// User is already logged in.
			return "You are already logged in";
		}
		
		// Check against user table
		$this->db->where('email', $email); 
		$query = $this->db->get("users");
		
		if ($query->num_rows() > 0) {
			$row = $query->row_array(); 
			
			// Check against password
			if(md5($password) != $row['password']) {
				return "Invalid password";
			}
			
			// Destroy old session
			$this->session->sess_destroy();
			
			// Create a fresh, brand new session
			$this->session->sess_create();
			
			// Remove the password field
			unset($row['password']);
			
			// Set session data
			$this->session->set_userdata(array("id" => $row["userID"]));
			
			// Set logged_in to true
			$this->session->set_userdata(array('logged_in' => true));			
			
			// Login was successful			
			return "no error";
		} 
		else {
			// No database result found
			return "Invalid email";
		}	
    }

    /*
	 * Allows user to modify profile information and returns error message or "no error"
	 *
	 * @access	public
	 * @param	array
	 * @return	string
	 */    
    public function updateProfile($params) {
    	// remove submit field from input array
        unset($params["submit"]);
    
    	// update the database with given information
        $this->db->where("userID", $this->session->userdata("id"));
        if(!$this->db->update("users", $params))
            return "Something went wrong... Please try again!";
            
        return
            "no error";
    }
}
    
    
?>
