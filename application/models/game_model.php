﻿<?
/*****************************************************************************
 * game_model.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Model file. Called by game.php and user.php controllers.
 * Includes methods for retrieving controller-requested data from SQL 
 * databases. Methods generally prepare SQL query statements based on 
 * controller input and then return the query results back to the controller.
 ****************************************************************************/

class Game_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    /*
	 * Checks if a kill password was correctly entered and returns error message or "no error"
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
    public function addKill($password) {
        $this->db->trans_start();
    
    	// find player's target
        $this->db->select("targetID");
        $this->db->where("playerID", $this->session->userdata("id"));
        $this->db->where("gameID", $this->session->userdata("game"));
        
        $query = $this->db->get("players");
        $result = $query->row_array();
        
        // find target's kill password
        $this->db->select("killPassword");        
        $this->db->where("playerID", $result["targetID"]);
        $this->db->where("gameID", $this->session->userdata("game"));
        
        $query = $this->db->get("players");
        $target = $query->row_array();
        
        $this->db->trans_complete();
        
        // check if kill passwords match
        if ($target["killPassword"] != $password) {
        	$this->db->trans_complete();
            return "Invalid kill password!";
        }
        
        else {
        	$this->db->trans_complete();
            return "no error";
        }
    }
    
    /*
	 * Allows administrator to send notificaiton to all players and returns error message or "no error"
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
    public function adminNotification($content) {
        if ($this->session->userdata("mode") != "admin")
            return "You cannot send a notification if you are not the administrator of this game.";
        
        if (!$this->notify($this->session->userdata("game"), 0, $content, date("Y-m-d H:i:s")))
            return "Something went wrong... Please try again.";
        
        return "no error";
    }
    
    
    /*
	 * Adds player to a game and returns error message or "no error"
	 *
	 * @access	public
	 * @param	string
	 * @param   string
	 * @return	string
	 */
    public function addPlayer($gameName, $gamePassword) {
        // find game with this name in database
        $this->db->where("name", $gameName);
        $query = $this->db->get("games");
        
        $results = $query->row_array();

        // check if game has already ended
        if ($results["endDate"] != NULL)
            return "This game has already ended";
        
        // check if game has already started  
        else if ($results["startDate"] != NULL)
            return "This game has already started"; 
        
        // check if user has entered the correct password
        else if ($gamePassword != $results["password"])
            return "Invalid password";
        
        // check if user is administering this game
        else if ($this->session->userdata("id") === $results["adminID"])
            return "You cannot join a game that you are administering.";        
                
        else {
            $this->db->trans_start();
            
            $array = array ("playerID" => $this->session->userdata("id"),
                            "gameID" => $results["gameID"],
                            
                            // target is null until admin decides to start the game
                            "targetID" => NULL,

                            // this will be randomly selected from a text file of words
                            "killPassword" => $this->selectKillPassword() );
            
            // check whether user is already playing this game            
            $this->db->where($array);
            $query = $this->db->get("players");
            
            if ($query->num_rows > 0)
                return "You have already joined this game.";
            
            
            // insert user into game
            $this->db->set($array); 
			if(!$this->db->insert("players")) {
				//There was a problem!
				return "Something went wrong... Please try again.";						
			}
			
			//$this->db->select("name");
			$this->db->where("userID", $this->session->userdata("id"));
			
			// notify the admin
			$query = $this->db->get("users");
			$user = $query->row_array();
			
			if (!$this->notify($results["gameID"], $results["adminID"], $user["name"] . " has joined the game.", date("Y-m-d H:i:s")))
			    return "Something went wrong... Please try again.";
			    
			$this->db->trans_complete();
			
        }
        
        // success!
        return "no error";
    }
    
    /*
	 * Creates a new game and inserts into database and returns error message or "no error"
	 *
	 * @access	public
	 * @param	array
	 * @return	string
	 */
    public function create($params) {
        // check if password and confirmed password match
        if ($params["gamePassword"] != $params["gamePassword2"])
            return "Passwords do not match.";
    
        // check if game name is already taken
        // in release, will allow duplicate names if other game with same name is already over
        $this->db->where("name", $params["gameName"]);
        $query = $this->db->get("games");
        
        if ($query->num_rows() > 0)
            return "Game name is already taken.";
        
        else {
            $array = array ("adminID" => $this->session->userdata("id"),
                            "name" => $params["gameName"],
                            "password" => $params["gamePassword"],
                            "killRules" => $params["gameKillingRules"],
                            "safetyRules" => $params["gameSafetyZones"],
                            "moreRules" => $params["gameAdditionalRules"] );
            
            // insert game into database
            $this->db->set($array);
            if (!$this->db->insert("games")) {
                // something went wrong!
                return "Something went wrong... Please try again.";
            }
            else $game = $this->db->insert_id();
        }
        
        // open game
        $this->setGame($game, $params["gameName"], "admin");
        
        // success!
        return "no error";
    }

    /*
	 * Retrieves list of kills made in a game or by a game player (depending on the mode)
	 *
	 * @access	public
	 * @return	array
	 */    
    public function getKills() {
    	// find kills in the database
        $this->db->select("killerID, victimID, time");
        
        // only select personal kills if in player mode
        if ($this->session->userdata("mode") == "player")
            $this->db->where("killerID", $this->session->userdata("id"));
            
        $this->db->where("gameID", $this->session->userdata("game"));
        $this->db->order_by("time", "desc");
        
        $query = $this->db->get("kills");
        $results = $query->result_array();
        
        $kills = array();
        
        // retrieve names for killers and victims
        foreach ($results as $kill) {
        	$killerName = $this->getPlayerName($kill["killerID"]);
        	
        	$victimName = $this->getPlayerName($kill["victimID"]);
        	
        	$kills[] = array ("killerName" => $killerName,
        					  "victimName" => $victimName,
        					  "time" => $kill["time"]);
        }
        
        return $kills;
    }

    /*
	 * Retrieves notifications
	 *
	 * @access	public
	 * @param	string
	 * @param   string
	 * @return	string
	 */    
    public function getNotifications() {
        $this->db->select("content, time");
        
        // find personal notifications
        $this->db->where("gameID", $this->session->userdata("game"));
        $this->db->where("playerID", $this->session->userdata("id"));
        
        // find notifications for everyone
        $this->db->or_where("playerID", 0);
        $this->db->where("gameID", $this->session->userdata("game"));
        
        $this->db->order_by("time", "desc");
        
        $query = $this->db->get("notifications");
        
        return $query->result_array();
    }

    /*
	 * Retrieves a player's kill password so that it can be displayed on their own device
	 *
	 * @access	public
	 * @return	string
	 */    
    public function getPassword() {
        $this->db->select("killPassword");
        
        $this->db->where("gameID", $this->session->userdata("game"));
        $this->db->where("playerID", $this->session->userdata("id"));
        
        $query = $this->db->get("players");
        $player = $query->row_array();
        
        return $player["killPassword"];
    }

    /*
	 * Retrieves all the players in a game
	 *
	 * @access	public
	 * @return	array
	 */    
    public function getPlayers() {
    	// players shouldn't be accessing this
        if ($this->session->userdata("mode") != "admin")
            return;
        
        // get IDs of players and their targets
        $this->db->select("playerID, targetID");
        $this->db->where("gameID", $this->session->userdata("game"));
        
        $query = $this->db->get("players");
        $pairings = $query->result_array();
        
        $players = array();
        
        // get names of players and their targets
        foreach ($pairings as $pair) {
        	$player = $this->getPlayerName($pair["playerID"]);
            
            if ($pair["targetID"] != NULL) 
            	$target = $this->getPlayerName($pair["targetID"]);
            
            else
                $target = NULL;
            
            $players[] = array("player" => $player, "target" => $target);
        }
        
        return $players;
    }

    /*
	 * Retrieves the name of a player, based on the ID of the player
	 *
	 * @access	public
	 * @param	int
	 * @return	string
	 */   
    private function getPlayerName($userID) {
    	$this->db->select("name");
    	
    	$this->db->where("userID", $userID);
    	
    	$query = $this->db->get("users");
    	$result = $query->row_array();
    	
    	return $result["name"];
    }
    
    /*
	 * Retrieves the rules of the current game.
	 *
	 * @access	public
	 * @param	int
	 * @return	bool
	 */
    public function getRules() {
        // check which game is open
        $gameID = $this->session->userdata("game");
        
        // get the rules from the database
        $this->db->select("killRules, safetyRules, moreRules, password, startDate");
        $this->db->from("games");
        $this->db->where("gameID", $gameID);
        
        $query = $this->db->get();
        
        $game = $query->row_array();
        
        if ($game["startDate"] != NULL)
            $started = true;
      	else
      			$started = false;
        
        unset($game["startDate"]);
        
        $game["started"] = $started;
        
        return $game;
    }

    /*
	 * To be executed when a kill has been made. Updates databases appropriately, checks if game has
	 * been won, and informs killer of his or her new target.
	 *
	 * @access	public
	 * @return	string
	 */    
    public function killSuccess() {
        $gameID = $this->session->userdata("game");
        $playerID = $this->session->userdata("id");
        
        $this->db->trans_start();
    	
    	// find out how many players are left in the game
        $this->db->where("gameID", $gameID);
        $this->db->where("place is NULL");
        $query = $this->db->get("players");
        
        $playerCount = $query->num_rows();
        
        // find the target's ID
        $this->db->select("targetID");
        $this->db->where("playerID", $playerID);
        $this->db->where("gameID", $gameID);
        
        $query = $this->db->get("players");
        $result = $query->row_array();
        
        $killedID = $result["targetID"];
        
        // find the target's target (who is the killer's new target)
        $this->db->select("targetID");
        $this->db->where("playerID", $killedID);
        $this->db->where("gameID", $gameID);
        
        $query = $this->db->get("players");
        $result = $query->row_array();
        
        $newTarget = $result["targetID"];
        
        // update target of killer
        $this->db->where("playerID", $playerID);
        $this->db->update("players", array("targetID" => $newTarget)); 
        
        // update place of victim
        $this->db->where("playerID", $killedID);
        $this->db->where("gameID", $gameID);
        $this->db->update("players", array ("place" => $playerCount, "targetID" => NULL));
         
        $killerName = $this->getPlayerName($playerID);
        $killedName = $this->getPlayerName($killedID);
        
		// find admin of the game, so they can be notified of the kill
        $this->db->select("adminID");
        $this->db->where("gameID", $gameID);
        $query = $this->db->get("games");
        $adminID = $query->row_array();
        
        $time = date("Y-m-d H:i:s");
        
        // insert the kill into the database
        $this->db->set(array ("gameID" => $gameID,
        					  "killerID" => $playerID,
        					  "victimID" => $killedID,
        					  "time" => $time) );
        $this->db->insert("kills");
        
        $this->db->trans_complete();
        
        // notify admin of the kill       
        $this->notify($gameID, 
                      $adminID["adminID"], 
                      $killerName . " has killed " . $killedName . 
                          ". There are now " . ($playerCount - 1) . " players left in the game." , 
                      $time);

        // notify the killer
        $this->notify($gameID, $playerID, "You have killed " . $killedName . ".", $time);
        
        // notify the target
        $this->notify($gameID, 
                      $killedID, 
                      "You were killed by " . $killerName . 
                          ". You placed number " . $playerCount . " in this game.", 
                      $time);
        
        // if only 2 players were left, this kill ended the game
        if ($playerCount == 2) {

            // notify the admin
            $this->notify($gameID, $adminID["adminID"], $killerName . " has won the game.", $time);
            
            // update the endDate of the game
            $this->db->where("gameID", $gameID);
            $this->db->update("games", array("endDate" => $time));

			// update the place of the winner
            $this->db->where("playerID", $playerID);
            $this->db->where("gameID", $gameID);
            $this->db->update("players", array("place" => 1));
            
            // notify the winner
            $this->notify($gameID, $playerID, "Congratulations, you are the winner!", $time);
            
            return "Congratulations, you have just won this game!";
        }
            
        else {
        	$newTargetName = $this->getPlayerName($newTarget);
            
            return "Success! Your new target is $newTargetName.";
        }
    }
    
    /*
	 * Adds notification to a game and returns true on success
	 *
	 * @access	public
	 * @param	int
	 * @param   int
	 * @param	string
	 * @param	datetime
	 * @return	boolean
	 */    
    private function notify($game, $for, $content, $time) {
        $array = array ("gameID" => $game,
                        "playerID" => $for,
                        "content" => $content,
                        "time" => $time );
        $this->db->set($array);
        
        if($this->db->insert("notifications"))
            return true;
        
        return false;
    } 
    
    /*
	 * Opens a requested game and checks whether user is player or admin and returns true if successful
	 *
	 * @access	public
	 * @param	int
	 * @return	bool
	 */
    public function open($gameID) {
        // check current user id
        $userID = $this->session->userdata("id");
        
        // check if user is administering this game
        $this->db->where("adminID", $userID);
        $this->db->where("gameID", $gameID);
        $query = $this->db->get("games");
        
        // if administering this game, open the game and set mode to be admin
        if ($query->num_rows() == 1) {
            $result = $query->row_array();
            $this->setGame($gameID, $result["name"], "admin");
            return true;
        }
        
        // check if user is playing this game    
        $this->db->select("*"); 
        $this->db->from("players");
        $this->db->join("games", "players.gameID = games.gameID");
        $this->db->where("players.playerID", $userID);
        $this->db->where("players.gameID", $gameID);
        $query = $this->db->get();
        
        // if playing this game, open the game and set mode to be player
        if ($query->num_rows() == 1) {
            $result = $query->row_array();
            $this->setGame($gameID, $result["name"], "player");
            return true;
        }
        
        // if we get to this point, something went wrong
        return false;
    }

    /*
	 * Randomly select a kill password when adding a player
	 *
	 * @access	private
	 * @return	string
	 */    
    private function selectKillPassword(){
        // parse password dictionary into a string
        $string = file_get_contents(base_url("welcome/words"));
        
        // explode the string into an array
        $passwords = explode(" ", $string);
        
        // select a random member of the array
        return $passwords[array_rand($passwords)];
        
    }

    /*
	 * Logs a user into a game
	 *
	 * @access	private
	 * @param	int
	 * @param	string
	 * @param   string
	 * @return	void
	 */    
    private function setGame($gameID, $gameName, $mode) {
    	// unset any previous game
        $unsetItems = array("game" => "", "gameName" => "", "mode" => "");
        $this->session->unset_userdata($unsetItems);
        
        // set the info for the new game
        $this->session->set_userdata(array ("game" => $gameID, "gameName" => $gameName, "mode" => $mode));
    }

    /*
	 * Start a game
	 *
	 * @access	public
	 * @param	string
	 * @param   string
	 * @return	void
	 */    
    public function start() {
    	// only the admin should be doing this
    	if ($this->session->userdata("mode") != "admin")
    		return;
    
        $gameID = $this->session->userdata("game");
        
        $this->db->trans_start();
    
        // fetch players
        $this->db->select("playerID");
        $this->db->where("gameID", $gameID);
        
        $query = $this->db->get("players");
        $targets = $query->result_array();
        $numPlayers = $query->num_rows();
        
        // if there's 1 or no players, there aren't enough for a proper game
        if ($numPlayers <= 1) {
            $this->notify($gameID, 
                            $this->session->userdata("id"), 
                            "The game failed to start because the number of players is $numPlayers.", 
                            date("Y-m-d H:i:s") );
            $this->db->trans_complete();
            return;
        }
            
        
        // assign targets
        
        // randomize the array of players
        shuffle($targets);
        
        // assign each player the next player from the random array as a target
        for ($i = 0; $i < $numPlayers; $i++) {
            $this->db->where("playerID", $targets[$i]["playerID"]);
            $this->db->where("gameID", $gameID);
            
            // prevent indexing out of bounds
            if ($i == 0)
            	$this->db->update("players", array("targetID" => $targets[$numPlayers-1]["playerID"]));
            
            else
	            $this->db->update("players", array("targetID" => $targets[$i-1]["playerID"]));
        }
        
        // set start time
        $time = date("Y-m-d H:i:s");
        $this->db->where("gameID", $gameID);
        $this->db->update("games", array("startDate" => $time));
        
        $this->db->trans_complete();
        
        // send out notification
        $this->notify($gameID, 0, "The game has started with $numPlayers players.", $time);
        
    }

    /*
	 * Determines the status of a game depending on whether its start and end dates are null
	 *
	 * @access	public
	 * @return	string
	 */    
    public function gameStatus() {
    	// find the start and end date of the game
        $this->db->select("startDate, endDate");
        $this->db->where("gameID", $this->session->userdata("game"));
        $query = $this->db->get("games");
        $game = $query->row_array();
        
        // the game has neither started nor ended
        if ($game["startDate"] == NULL && $game["endDate"] == NULL)
        	return "before";
        // the game has started but not ended
    	else if ($game["startDate"] != NULL && $game["endDate"] == NULL)
    		return "during";
    	// the game has both started and ended
    	else if ($game["startDate"] != NULL && $game["endDate"] != NULL)
    		return "after";

    }
    
    /*
	 * Allows admin to edit the rules and returns error message or "no error"
	 *
	 * @access	public
	 * @param	array
	 * @return	string
	 */    
    public function updateRules($params) {
    	// only the admin should be editing the rules
    	if ($this->session->userdata("mode") != "admin")
    		return "You are not the administrator of this game!";
    
    	// remove the form submission input
        unset($params["submit"]);
    
    	// update the database
        $this->db->where("gameID", $this->session->userdata("game"));
        if(!$this->db->update("games", $params))
            return "Something went wrong... Please try again!";
        
        // notify all players that the rules have been changed
        $this->notify($this->session->userdata("game"), 0, "The rules of this game have been updated by the administrator.", date("Y-m-d H:i:s"));
            
        return
            "no error";
    }
}
    
?>