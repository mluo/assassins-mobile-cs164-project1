﻿<?/*****************************************************************************
 * words.php
 * 
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Space-separated list of words from which selectKillPassword() in
 * game_model.php randomly selects a kill password for each player.
 *******************************************************************************/?>
happy sleepy dopey bashful sneezy doc grumpy nemo dorey pizza otto noch's carbon nitrogen oxygen boron hydrogen potassium ozone knife dagger gun poison water bottle comedian vodka gin martini margarita spinach basil salmon rum coke pepsi monitor lamp computer apple orange grape pen pencil paper eraser wall ceiling ground floor hotel park city town suburb peanut butter jelly toast bread sourdough friendship bracelet necklace earring ring freedom liberty constitution election president sloth sin envy greed gluttony wrath lust pride red green purple violet yellow dog cat lizard sock shirt cashews password 12345 peanuts almonds fan elephant champagne razor accio aguamenti alohomora anapneo confundo crucio expelliarmus rictusempra reparo riddikulus jigglypuff charmander bulbasaur ivysaur blastoise pikachu raichu geodude diglet dugtrio squirtle chancy chauncy meyer dumbo aurora magikarp snorlax