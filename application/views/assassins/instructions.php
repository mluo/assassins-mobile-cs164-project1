<?/*****************************************************************************
 * assassins/instructions.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Provides instructions for the website.
 ****************************************************************************/?>
<h2 align="center">Welcome</h2>
<p>
    Welcome to Assassins Mobile! We hope this website will help you facilitate your next game of Assassins,
    whether it be at your college, your company, or wherever you can herd up a bunch of blood-thirsty, back-stabbing friends!.
    This page is here to guide you through this website-- we assume you already know how to play the game of Assassins.
</p>

<h2 align="center">Getting Started</h2>
<p>
    To get started, please register and log in. With one account, you can both join games and create them. After logging in,
    you will be brought to the home page, where you can access all of you games, create a game, join game, and do other
    cool stuff like view your profile and game history.
</p>

<h2 align="center">Creating a Game</h2>
<p>
    To create a game, click on the "Create a Game" link from the home page. After filling out the details, you will enter
    the administration area for the game, from which you can monitor the progress of the game and send out notifications
    to all the players. You can access the administration area for each game you are administrating from the home page. 
    To have actual players join the game, you must distribute the game's name and password (which you 
    stipulated when you created the game). After you think enough players have joined the game, you simply click the
    "Start Game" button on the "Rules" page to start the game!
</p>

<h2 align="center">Playing the Game</h2>
<p>
    To join a game, you must have obtained the game's name and password from the adminstrator of the game. Click on the
    "Join a Game" link from the home page, enter this information, and you will be brought to the player area for the game,
    where you can view your target, log kills, see a history of your kills for that game, view the rules of the game, 
    and see the notifications for the game (e.g. updates on how many players are left, messages from the administrator). 
    To log a kill, you must obtain your target's "kill password," which your target can find on his or her rules page, and which your
    target should willingly give to you if you kill him or her. After you succesfully log a kill, the website will
    automatically update your target. The game continues like this until someone wins, at which point the game and its 
    results will appear in your game history, which can be accessed from the home page.
</p>

<h2 align="center">Have fun!</h2>
<p>
    Disclaimer: we claim no responsibilities for injuries or other losses sustained while playing Assassins 
    using this website.
</p>

<!-- back button -->
<a align="center" href="<?= base_url('user/login') ?>" data-ajax="false" data-role="button" data-theme="a">Back</a>
