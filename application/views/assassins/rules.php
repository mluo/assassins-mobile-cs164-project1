<?/*****************************************************************************
 * assassins/rules.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays rules of current game as provided by games controller. 
 ****************************************************************************/?>
 
<h2 align="center">Game Rules</h2>

<br>

<!-- display list of rules -->
<ul data-role="listview" data-theme="a" data-dividertheme="a">
    <li data-role="list-divider">Rules About Killing</li>
    <li><?= $game["killRules"] ?></li>
    
    <li data-role="list-divider">Rules About Safety Zones</li>
    <li><?= $game["safetyRules"] ?></li>
    
    <li data-role="list-divider">Additional Rules</li>
    <li><?= $game["moreRules"] ?></li>
    
    <li data-role="list-divider">Game Password</li>
    <li><?= $game["password"] ?></li>    
    
    <!-- show kill password if player -->
    <? if ($mode == "player"): ?>
    <li data-role="list-divider">Kill Password</li>
    <li><?= $killPassword ?></li>
    <? endif ?>
</ul>

<br><br>

<!-- link to start game if game has not started and page accessed by adminstrator -->
<? if (!$game["started"]  && $mode == "admin"): ?>
<a href="<?= base_url('game/startGame') ?>" data-ajax="false" data-role="button" data-theme="a">Start Game!</a>
<? endif ?>

<!-- only administrator of game can edit the rules -->
<? if ($mode == "admin"): ?>
    <a href="<?= base_url('game/editRules') ?>" data-ajax="false" data-role="button" data-theme="a">Edit Rules</a>
<? endif ?>
