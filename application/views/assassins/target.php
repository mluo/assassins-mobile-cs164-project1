<?/*****************************************************************************
 * assassins/target.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. For players. Displays the profile of the player's current target.
 *******************************************************************************/?>
 
﻿<div align="center"><h2> Kill This Person: </h2></div>

<br>

<!-- check if game has started, i.e. a target has been set -->
<? if ($gameStatus === "during" && isset($targetData["name"])) : ?>
		<!-- display profile info of target -->
		<ul data-role="listview" data-theme="a" data-dividertheme="a">
				<li data-role="list-divider">Name</li>
				<li><?= $targetData["name"] ?></li>
				<li data-role="list-divider">Email</li>
				<li><?= $targetData["email"] ?></li>
				<li data-role="list-divider">Physical Description</li>
				<li><?= $targetData["desc"] ?></li>
				<li data-role="list-divider">Location</li>
				<li><?= $targetData["loc"] ?></li>
		</ul>

<!-- if game has started but target is NULL, player has been eliminated -->
<? elseif ($gameStatus === "during" && !isset($targetData["name"])) : ?>
		<br><br>
		<div align="center">You have been eliminated from this game already!</div>

<!-- if game is over and target is NULL, you did not win -->
<? elseif($gameStatus === "after" && !isset($targetData["name"])) : ?>
		<br><br>
		<div align="center">This game is over already.</div>

<!-- if game is over and target is set (to yourself), you won -->
<? elseif ($gameStatus === "after" && isset($targetData["name"])) : ?>
		<br><br>
		<div align="center">You have won this game already!</div>

<!-- if game has not started yet -->
<? elseif ($gameStatus === "before"): ?>
		<br><br>
		<div align="center">Patience... this game has not started yet!</div>
		
<? endif ?>