<?/*****************************************************************************
 * assassins/profile.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays profile of user. Accessed from home.php. 
 ****************************************************************************/?>
 
<div align="center"><h2> Your Profile </h2></div>

<br>

<!-- display profile information -->
<ul data-role="listview" data-theme="a" data-dividertheme="a">
    <li data-role="list-divider">Name</li>
    <li><?= $playerData["name"] ?></li>
    
    <li data-role="list-divider">Email</li>
    <li><?= $playerData["email"]  ?></li>
    
    <li data-role="list-divider">Physical Description</li>
    <li><?= $playerData["desc"]  ?></li>
    
    <li data-role="list-divider">Location</li>
    <li><?= $playerData["loc"]  ?></li>
</ul>

<!-- link to edit profile -->
<br><br>
<a href="<?= base_url('user/editProfile') ?>" data-ajax="false" data-role="button" data-theme="a">Edit Profile</a>

