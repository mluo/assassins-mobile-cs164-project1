<?/*****************************************************************************
 * assassins/login.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Index page. View file. Displays form for logging in and sends information to 
 * user controller. If successful, redirects to home page. Otherwise displays 
 * error message. Also has links to registration and game instruction.
 ****************************************************************************/?>

<div align="center">
    <h2> Log In </h2>
    
    <!-- placeholder for error message (invalid login, etc.) passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- login form -->
    <form method="post" action="<?= base_url('user/login') ?>" id="login" data-ajax="false">
        <label for="email" class="ui-hidden-accessible">Email:</label>
        <input type="email" name="email" id="email"  value="" required="required" placeholder="Email Address"/>

        <label for="password" class="ui-hidden-accessible">Password:</label>
        <input type="password" name="password" id="password"  value="" required="required" placeholder="Password"/>

        <label for="submit" class="ui-hidden-accessible">Log In</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Log In" />
    </form>
    
    <!-- registration link -->
    <h2> New user? </h2>
    <a href="<?= base_url('user/register') ?>" data-role="button" data-ajax="false" data-theme="a" >Register</a>
    
    <!-- link to game instructions -->
    <h2> How to Use </h2>
    <a href="<?= base_url('game/instructions') ?>" data-role="button" data-ajax="false" data-theme="a" >Website Instructions</a>    
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            // make sure email not blank
            if ($("#email").val() == "") {
                $("#email").attr("placeholder", "You must enter your email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }
            // make sure email is of correct format
            else if ( !emailReg.test( $("#email").val() ) ) {
                $("#email").attr("placeholder", "Invalid email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }  
            // make sure password is not blank
            if ($("#password").val() == "") {
                $("#password").attr("placeholder", "You must enter your password!");
                $("#password").attr("value", "");
                event.preventDefault();
            }           
        });
    });   
</script>
        
