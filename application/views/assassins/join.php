<?/*****************************************************************************
 * assassins/join.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays form for joining a game and sends information to user
 * controller. If successful, redirects to home page. Otherwise displays error 
 * message.
 ****************************************************************************/?>

<div align="center">
    <span> Please enter information provided by game administrator. </span>
    
    <br>
    
    <!-- placeholder for error message passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- to join a game as a player, user fills out this form with the game name and password, provided by admin -->
    <form method="post" action="<?= base_url('user/join') ?>" id="join" data-ajax="false">
        <label for="gameName" class="ui-hidden-accessible">Name:</label>
        <input type="text" name="gameName" id="gameName"  value="" required="required" placeholder="Game Name"/>
        
        <!-- game password is not sensitive enough to merit input type of password; plaintext okay -->
        <label for="gamePassword" class="ui-hidden-accessible">Password:</label>
        <input type="text" name="gamePassword" id="gamePassword"  value="" required="required" placeholder="Game Password"/>

        <label for="submit" class="ui-hidden-accessible">Login</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Join!" />
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){
            // make sure gameName not blank
            if ($("#gameName").val() == "") {
                $("#gameName").attr("placeholder", "You must enter the name of the game!");
                $("#gameName").attr("value", "");
                event.preventDefault();
            }
            // make sure gamePassword is not blank
            if ($("#gamePassword").val() == "") {
                $("#gamePassword").attr("placeholder", "You must enter the game's password!");
                $("#gamePassword").attr("value", "");
                event.preventDefault();
            }           
        });
    });   
</script>
        
