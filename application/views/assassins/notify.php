<?/*****************************************************************************
 * assassins/notify.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Allows adminstrator to add a custom notification to the game.
 ****************************************************************************/?>

<div align="center">
    <span> Fill out this form to send a notification to all players in the game: </span>
    
    <!-- placeholder for error message passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- form for administrator to add a custom notification to the database -->
    <form method="post" action="<?= base_url('game/notify') ?>" id="notify" accept-charset="utf-8" data-ajax="false">   
        <label for="content" class="ui-hidden-accessible">Location:</label>
        <textarea type="text" name="content" id="content" value="" placeholder="Message" required="required"></textarea> 

        <label for="submit" class="ui-hidden-accessible">Send</label>
        <input type="submit" formnovalidate="formnovalidate" data-theme="a" name="submit" id="submit" value="Send!" />
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){
            // make sure message is not blank
            if ($("#content").val() == "") {
                $("#content").attr("placeholder", "Please enter a message!");
                $("#content").attr("value", "");
                event.preventDefault();
            }    
        });
    });   
</script>
        
