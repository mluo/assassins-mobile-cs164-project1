<?/*****************************************************************************
 * assassins/create.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays form for creating a game and sends information to game
 * controller. If successful, redirects to game rules. Otherwise displays error 
 * message.
 ****************************************************************************/?>
 
 <div align="center">
    <span> Please enter the following information to create a game. Prospective players will need the game's name and password to join.</span>
    
    <br>
    
    <!-- placeholder for error message if passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- form for game information that potential admin enters to create game -->
    <form method="post" action="<?= base_url('game/create') ?>" id="create" data-ajax="false">
        <label for="gameName" class="ui-hidden-accessible">Name:</label>
        <input type="text" name="gameName" id="gameName"  value="" required="required" placeholder="Game Name"/>
        
        <!-- game password is not sensitive; plaintext okay -->
        <label for="gamePassword" class="ui-hidden-accessible">Password:</label>
        <input type="text" name="gamePassword" id="gamePassword"  value="" required="required" placeholder="Game Password"/>
        
        <!-- make admin confirm password -->
        <label for="gamePassword2" class="ui-hidden-accessible">Confirm Password:</label>
        <input type="text" name="gamePassword2" id="gamePassword2"  value="" required="required" placeholder="Confirm Password"/>
        
        <label for="gameKillingRules" class="ui-hidden-accessible">Killing Rules: </label>
        <textarea name="gameKillingRules" id="gameKillingRules" required="required" placeholder="Rules Governing How to Kill Target"></textarea>
        
        <label for="gameSafetyZones" class="ui-hidden-accessible">Safety Zones: </label>
        <textarea name="gameSafetyZones" id="gameSafetyZones" required="required" placeholder="Rules Governing Safety Zones"></textarea>
         
        <!-- additional rules are not required -->
        <label for="gameAdditionalRules" class="ui-hidden-accessible">Additional Rules: </label>
        <textarea name="gameAdditionalRules" id="gameAdditionalRules" required="required" placeholder="Additional Rules"></textarea> 
        
        <label for="submit" class="ui-hidden-accessible">Create</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Create!"></button>
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){
            // make sure name not blank
            if ($("#gameName").val() == "") {
                $("#gameName").attr("placeholder", "You must specify the game's name!");
                $("#gameName").attr("value", "");
                event.preventDefault();
            }
            // make sure password is not blank
            if ($("#gamePassword").val() == "") {
                $("#gamePassword").attr("placeholder", "You must specify the game's password!");
                $("#gamePassword").attr("value", "");
                event.preventDefault();
            }  
            
            // make sure password2 is not blank
            if ($("#gamePassword2").val() == "") {
                $("#gamePassword2").attr("placeholder", "You must re-enter the password!");
                $("#gamePassword2").attr("value", "");
                event.preventDefault();
            }  
            // make sure passwords match
            else if ($("#gamePassword2").val() != $("#gamePassword").val()) {
                $("#gamePassword2").attr("placeholder", "The two passwords don't match!");
                $("#gamePassword2").attr("value", "");
                event.preventDefault();
            } 
            // make sure rules have been set for killing
            if ($("#gameKillingRules").val() == "") {
                $("#gameKillingRules").attr("placeholder", "You must set rules governing killing!");
                $("#gameKillingRules").attr("value", "");
                event.preventDefault();
            }
            // make sure rules have been set about safety zones
            if ($("#gameSafetyZones").val() == "") {
                $("#gameSafetyZones").attr("placeholder", "You must enter rules governing safety zones!");
                $("#gameSafetyZones").attr("value", "");
                event.preventDefault();
            }    
        });
    });   
</script>
        
