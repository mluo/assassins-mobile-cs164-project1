<?/*****************************************************************************
 * assassins/kills.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays list of kills, with victim name, timestamp, and 
 * killer name (only if in admin mode).
 ****************************************************************************/?>

<!-- title of page depends on whether admin or player is viewing -->
<? if ($mode == "player"): ?>
    <h2 align="center"> Your Kills </h2>
<? else: ?>
    <h2 align="center"> Logged Kills </h2> 
<? endif ?>
 
<!-- table of the kills, with the killer column only if admin is viewing -->
<table style="width:100%">
    <thead>
        <tr>
            <!-- column headers -->
            <th style="width:33%" scope="col">Victim:</th>
            
            <? if ($mode == "admin"): ?>
                <th style="width:33%" scope="col">Killer:</th>
            <? endif ?>
            
            <th style="width:33%" scope="col">Timestamp:</th>
        </tr>
    </thead>
    
    <tbody>
        <? foreach ($kills as $kill): ?>
            <tr>
                <th scope="row"><?= $kill["victimName"] ?></th>
                
                <? if ($mode == "admin"): ?>
                    <td align="center"><?= $kill["killerName"] ?></td>     
                <? endif ?>
                
                <td align="center"><?= $kill["time"] ?></td>   
            </tr>
        <? endforeach ?>              
    </tbody>
</table>
