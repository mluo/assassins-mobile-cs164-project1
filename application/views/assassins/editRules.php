<?/*****************************************************************************
 * assassins/editRules.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays form with current game rules prepopulated. Sends
 * information to game controller. Only accessible to the admin of a game.
 ****************************************************************************/?>
 
<div align="center">
    <!-- placeholder for error message if passed by controller-->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    <!-- form for editing game rules -->
    <form method="post" action="<?= base_url('game/editRules') ?>" id="editRules" data-ajax="false">        
        <label for="killRules">Rules on Killing: </label>
        <!-- prepopulate the text areas with current rules -->
        <textarea name="killRules" id="killRules" required="required" ><?= $rules["killRules"] ?></textarea>
        
        <label for="safetyRules">Rules about Safety Zones: </label>
        <textarea name="safetyRules" id="safetyRules" required="required"><?= $rules["safetyRules"] ?></textarea>
         
        <label for="moreRules">Additional Rules: </label>
        <textarea name="moreRules" id="moreRules" required="required"><?= $rules["moreRules"] ?></textarea> 
        <br>
        <label for="submit" class="ui-hidden-accessible">Submit Edits</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Submit Edits"></button>
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){          
            // make sure rules have been set for killing
            if ($("#gameKillingRules").val() == "") {
                $("#gameKillingRules").attr("placeholder", "You must set rules governing killing!");
                $("#gameKillingRules").attr("value", "");
                event.preventDefault();
            }
            // make sure rules have been set about safety zones
            if ($("#gameSafetyZones").val() == "") {
                $("#gameSafetyZones").attr("placeholder", "You must enter rules governing safety zones!");
                $("#gameSafetyZones").attr("value", "");
                event.preventDefault();
            }    
        });
    });   
</script>
        
