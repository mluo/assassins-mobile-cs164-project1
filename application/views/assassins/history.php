<?/*****************************************************************************
 * assassins/history.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays list of completed games that the user either played in 
 * or administrated, their outcomes, and a link to the game's pages.  
 ****************************************************************************/?>
<div data-role="collapsible-set">

	<!-- collapsible table of games played, with game name and place achieved -->
	<div data-role="collapsible">
	    <h3>Games You Played</h3>
        <table style="width:100%; color:white">
            <thead>
                <tr>
                    <!-- column headers -->
                    <th scope="col">Game Name:</th>
                    <th scope="col">Place:</th>
                    <th style="width:25%" scope="col">Start Date:</th>
                    <th style="width:25%" scope="col">End Date:</th>
                    
                </tr>
            </thead>
            
            <tbody>
                <? foreach ($playedGames as $game): ?>
                    <tr>
                    		<!-- link to game's pages -->
                        <th scope="row"><a href="<?= base_url('game/open/' . $game["gameID"]) ?>"><?= $game["name"] ?></a></th>
                        <td align="center"><?= $game["place"] ?></td>     
                        <td><?= $game["startDate"] ?></td>   
                        <td><?= $game["endDate"] ?></td>   
                    </tr>
                <? endforeach ?>              
            </tbody>
        </table>
	</div>
	
	<!-- collapsible table of games administrated, with game name and winner -->
	<div data-role="collapsible">
	    <h3>Games You Administrated</h3>
        <table style="width:100%; color:white">
            <thead>
                <tr>
                    <th scope="col">Game Name:</th>
                    <th scope="col">Winner:</th>
                    <th style="width:25%" scope="col">Start Date:</th>
                    <th style="width:25%" scope="col">End Date:</th>
                </tr>
            </thead>
            
            <tbody>
                <? foreach ($adminedGames as $game): ?>
                    <tr>
                        <th scope="row"><a href="<?= base_url('game/open/' . $game["gameID"]) ?>"><?= $game["name"] ?></a></th>
                        <td><?= $game["winner"] ?></td>     
                        <td><?= $game["startDate"] ?></td>   
                        <td><?= $game["endDate"] ?></td>  
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>
	</div>
	
</div>


    
        
        
        
