<?/*****************************************************************************
 * assassins/register.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays form for registering and sends information to user
 * controller. If successful, redirects to home page. Otherwise displays error 
 * message.
 ****************************************************************************/?>

<div align="center">
    <h2> Register </h2>
    
    <!-- placeholder for error message passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- form for a prospective user to register -->
    <form method="post" action="<?= base_url('user/register') ?>" id="register" accept-charset="utf-8" data-ajax="false">   
        <label for="name" class="ui-hidden-accessible">Name:</label>
        <input type="text" name="name" id="name" value="" placeholder="Name" required="required"  />  
            
        <label for="email" class="ui-hidden-accessible">Email:</label>
        <input type="email" name="email" id="email"  value="" required="required" placeholder="Email Address"/>

        <label for="password" class="ui-hidden-accessible">Password:</label>
        <input type="password" name="password" id="password"  value="" required="required" placeholder="Password"/>
        
        <!-- make user confirm his/her password -->
        <label for="password2" class="ui-hidden-accessible">Confirm Password:</label>
        <input type="password" name="password2" id="password2"  value="" required="required" placeholder="Confirm Password"/>
        
        <label for="description" class="ui-hidden-accessible">Physical Description: </label>
        <textarea name="description" id="description"  required="required" placeholder="Physical Description of Yourself"></textarea>
        
        <label for="location" class="ui-hidden-accessible">Location:</label>
        <input type="text" name="location" id="location" value="" placeholder="Location" required="required"  />  

        <label for="submit" class="ui-hidden-accessible">Register</label>
        <input type="submit" formnovalidate="formnovalidate" data-theme="a" name="submit" id="submit" value="Register" />
    </form>
    
    <!-- cancel button -->
    <a href="<?= base_url('user/login') ?>" data-ajax="false" data-role="button" data-theme="a">Cancel</a>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            // make sure name not blank
            if ($("#name").val() == "") {
                $("#name").attr("placeholder", "You must enter your name!");
                $("#name").attr("value", "");
                event.preventDefault();
            }
            // make sure email not blank
            if ($("#email").val() == "") {
                $("#email").attr("placeholder", "You must enter your email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }
            // make sure email is of correct format
            else if ( !emailReg.test( $("#email").val() ) ) {
                $("#email").attr("placeholder", "Invalid email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }  
            // make sure password is not blank
            if ($("#password").val() == "") {
                $("#password").attr("placeholder", "You must enter your password!");
                $("#password").attr("value", "");
                event.preventDefault();
            }  
            // make sure password2 is not blank
            if ($("#password2").val() == "") {
                $("#password2").attr("placeholder", "You must re-enter your password!");
                $("#password2").attr("value", "");
                event.preventDefault();
            }  
            // make sure passwords match
            else if ($("#password2").val() != $("#password").val()) {
                $("#password2").attr("placeholder", "Your passwords don't match!");
                $("#password2").attr("value", "");
                event.preventDefault();
            } 
            // make sure description not blank
            if ($("#description").val().length < 20) {
                $("#description").attr("placeholder", "You must enter a longer description!");
                $("#description").attr("value", "");
                event.preventDefault();
            }
            // make sure location not blank
            if ($("#location").val() == "") {
                $("#location").attr("placeholder", "You must enter your location!");
                $("#location").attr("value", "");
                event.preventDefault();
            }    
        });
    });   
</script>
        
