<?/*****************************************************************************
 * assassins/editProfile.php
 * 
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays form with user profile information fields prepopulated. 
 * Sends information to user controller. Accessible from profile.php.
 ****************************************************************************/?>
 
<div align="center">
    <!-- placeholder for error message if passed by controller-->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- form for editing user profile-->
    <form method="post" action="<?= base_url('user/editProfile') ?>" id="editProfile" data-ajax="false">        
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="<?= $playerData['name'] ?>" placeholder="<?= $playerData['name'] ?>" required="required"  />  
        
        <!-- don't allow editing of email -->
        <br>
        
        <p>Email: &nbsp; <?= $playerData['email'] ?></p>
        
        <br>

        <label for="desc">Physical Description: </label>
        <textarea name="desc" id="desc" required="required"><?= $playerData["desc"] ?></textarea> 
        
        <label for="loc">Location: </label>
        <textarea name="loc" id="loc" required="required"><?= $playerData["loc"] ?></textarea> 
        
        <br>
        
        <label for="submit" class="ui-hidden-accessible">Submit Edits</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Submit Edits"></button>
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){          
            // make sure name not blank
            if ($("#name").val() == "") {
                $("#name").attr("placeholder", "You must enter a name!");
                $("#name").attr("value", "");
                event.preventDefault();
            }
            // make sure email not blank
            if ($("#email").val() == "") {
                $("#email").attr("placeholder", "You must enter an email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }
            // make sure email is of correct format
            else if ( !emailReg.test( $("#email").val() ) ) {
                $("#email").attr("placeholder", "Invalid email address!");
                $("#email").attr("value", "");
                event.preventDefault();
            }
            // make sure description not blank
            if ($("#description").val().length < 20) {
                $("#description").attr("placeholder", "You must enter a longer description!");
                $("#description").attr("value", "");
                event.preventDefault();
            }
            // make sure location not blank
            if ($("#location").val() == "") {
                $("#location").attr("placeholder", "You must enter your location!");
                $("#location").attr("value", "");
                event.preventDefault();   
        });
    });   
</script>
        
