<?/*****************************************************************************
 * assassins/kill.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Allows player to report that he has killed his target by 
 * entering the "Kill Password" of his target. Each player knows his own
 * "Kill Password" (located on the his profile page). 
 ****************************************************************************/?>

<div align="center">
    <span> If you have killed your target, please ask for his or her "kill password" and enter it here. </span>
    
    <br>
    
    <!-- placeholder for error message passed by controller -->
    <span style="color:#fa9f20"><?= $errorMessage ?></span>
    
    <!-- to join a game as a player, user fills out this form with the game name and password, provided by admin -->
    <form method="post" action="<?= base_url('game/logKill') ?>" id="join" data-ajax="false"> 
        <!-- kill password is not sensitive enough to merit input type of password; plaintext okay -->
        <label for="killPassword" class="ui-hidden-accessible">Password:</label>
        <input type="text" name="killPassword" id="killPassword"  value="" required="required" placeholder="Kill Password"/>

        <label for="submit" class="ui-hidden-accessible">Kill</label>
        <input type="submit" formnovalidate="formnovalidate"  data-theme="a" name="submit" id="submit" value="Kill!" />
    </form>
</div>

<!-- pre-validate user input -->
<script>
    $(document).ready(function(){
        $("#submit").click(function(event){ 
            // make sure killPassword is not blank
            if ($("#killPassword").val() == "") {
                $("#killPassword").attr("placeholder", "You must enter the kill password!");
                $("#killPassword").attr("value", "");
                event.preventDefault();
            }           
        });
    });   
</script>
        
