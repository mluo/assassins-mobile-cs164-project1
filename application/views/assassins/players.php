<?/*****************************************************************************
 * assassins/players.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. For admins. Displays list of all players in game with their 
 * current target. Also indicates if a player is dead or the winner.
 ****************************************************************************/?>
 
<h2 align="center">Players</h2> 
<table style="width:100%">
    <thead>
        <tr>
            <!-- column headers -->
            <th style="width:50%" scope="col">Player:</th>
            <th style="width:50%" scope="col">Current Target:</th>
        </tr>
    </thead>
    
    <tbody>
        <? foreach ($players as $matchup): ?>

            <tr>
                
                <!-- if player is dead or winner, change color to red or green-->
                <th 
                <? 
                if ($matchup["target"] == NULL && $gameStatus != "before") 
                		echo "style='color:#6c2123'"; 
                elseif ($matchup["target"] == $matchup["player"]) 
                		echo "style='color:green'"; 
                ?> 
                    scope="row"><?= $matchup["player"] ?></th>
    
                <!-- if player is dead or winner, indicate in target column -->
                <? if ($matchup["target"] == $matchup["player"]): ?>
                		<td align="center" style="color:green">Winner</td>   
                <? elseif ($matchup["target"] == NULL && $gameStatus != "before"): ?>
                    <td align="center" style="color:#6c2123">Eliminated</td>   
                <? else: ?>
                    <td align="center"><?= $matchup["target"] ?></td>   
                <? endif ?>
                
            </tr>
            
        <? endforeach ?>              
    </tbody>
</table>
