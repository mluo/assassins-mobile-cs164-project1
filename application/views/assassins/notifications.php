<?/*****************************************************************************
 * assassins/notifications.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View files. Displays list of notifications for a game.
 *******************************************************************************/?>
 <h2 align="center">Notifications</h2>
 
 <br>
 
 <ul data-role="listview" data-theme="a" data-dividertheme="a">
    <? foreach ($notifications as $notification): ?>
        <li>
            <p><?= $notification["time"] ?></p>
            <strong><?= $notification["content"] ?></strong>
            
        </li>
    <? endforeach ?>
</ul>

