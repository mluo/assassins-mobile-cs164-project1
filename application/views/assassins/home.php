<?/*****************************************************************************
 * assassins/home.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * View file. Displays home page with games playing and administering and links
 * to create game, join game, view profile, view history.
 ****************************************************************************/?>

<div align="center"><h2> Choose a Game </h2></div>

<br>

<ul data-role="listview" data-theme="a" data-dividertheme="a" data-mini="true">
    <!-- list of links to games that you are playing -->
    <li data-role="list-divider" data-mini="true">Games Playing</li>
    <? foreach ($playerGames as $game): ?>
       <li><a href="<?= base_url('game/open/' . $game['gameID']) ?>" data-ajax="false"><?= $game["name"] ?></a></li>
    <? endforeach ?>
    
    <!-- list of links to games that you are administrating -->
    <li data-role="list-divider" data-mini="true">Games Administrating</li>
    <? foreach ($adminGames as $game): ?>
        <li><a href="<?= base_url('game/open/' . $game['gameID']) ?>" data-ajax="false"><?= $game["name"] ?></a></li>
    <? endforeach ?>
</ul>   

<br>

<!-- links to other pages and forms -->
<div align="center"><h2> Other Options </h2></div>
<div data-role="controlgroup">
    <a href="<?= base_url('game/create') ?>" data-ajax="false" data-role="button" data-theme="a">Create a Game</a>
    <a href="<?= base_url('user/join') ?>" data-ajax="false" data-role="button" data-theme="a">Join a Game</a>
    <a href="<?= base_url('user/profile') ?>" data-ajax="false" data-role="button" data-theme="a">View Profile</a>
    <a href="<?= base_url('user/history') ?>" data-ajax="false" data-role="button" data-theme="a">View Game History</a>
    <a href="<?= base_url('user/logout') ?>" data-ajax="false" data-role="button" data-theme="a">Log Out</a>
    <a href="<?= base_url('game/instructions') ?>" data-ajax="false" data-role="button" data-theme="a">Website Instructions</a>
</div>   

