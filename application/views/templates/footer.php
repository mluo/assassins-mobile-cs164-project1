<?/********************************************************************
 * templates/footer.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 1
 *
 * Default footer to be used on every page. Displays 5 button options 
 * (different for admin vs. player) or no buttons at all (homepage,
 * login, registration, etc).
 *********************************************************************/?>
<? if ($mode == "player"): ?>
            </div> <!-- content -->
	        <br>
	        <div data-role="footer" data-id="myfooter" data-position="fixed" data-theme="a">
	            <div data-role="navbar" data-theme="a">
	                <ul>
		                <li><a href="<?= base_url('user/target') ?>" data-icon="star" data-ajax="false">
			                Target
		                </a></li>
		                <li><a href="<?= base_url('game/kills/') ?>" data-icon="check" data-ajax="false">
			                Kills
		                </a></li>
		                <li><a href="<?= base_url('game/logKill') ?>" data-icon="plus" data-ajax="false">
			                Log Kill
		                </a></li>
		                <li><a href="<?= base_url('game/rules/') ?>" data-icon="gear" data-ajax="false">
			                Rules
		                </a></li>
		                <li><a href="<?= base_url('game/notifications/') ?>" data-icon="info" data-ajax="false">
			                Notifications
		                </a></li>
		            </ul>
	            </div>
	        </div>
	    </div><!-- page -->
	</body>
</html>	      
		      
<? elseif ($mode == "admin"): ?>	
            </div> <!-- content -->	      
	        <br>
	        <div data-role="footer" data-id="myfooter" data-position="fixed" data-theme="a">
	            <div data-role="navbar" data-theme="a">
	                <ul>
		                <li><a href="<?= base_url('game/players') ?>" data-icon="grid" data-ajax="false">
			                Players
		                </a></li>
		                <li><a href="<?= base_url('game/kills/') ?>" data-icon="check" data-ajax="false">
			                Kills
		                </a></li>
		                <li><a href="<?= base_url('game/rules/') ?>" data-icon="gear" data-ajax="false">
			                Rules
		                </a></li>
		                <li><a href="<?= base_url('game/notifications/') ?>" data-icon="info" data-ajax="false">
			                Notifications
		                </a></li>
		                <li><a href="<?= base_url('game/notify') ?>" data-icon="alert" data-ajax="false">
			                Notify Players
		                </a></li>
		            </ul>
	            </div>
	        </div>
	    </div><!-- page -->
	</body>
</html>

<? else: ?>
            </div><!-- content -->
        </div><!-- page -->
	</body>
</html>
<? endif; ?>
