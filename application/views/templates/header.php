<?/********************************************************************
 * templates/header.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 1
 *
 * Default header to be used on every page.
 *********************************************************************/?>

<? $this->load->helper('url') ?>

<!DOCTYPE html>
<html>
	<head> 
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= htmlspecialchars($title) ?></title> 
		
        <!-- jQuery Mobile Theme - CS164 STAFF: PLEASE CHANGE THE BELOW HREF ACCORDINGLY -->
        <link rel="stylesheet" href="http://project1/themes/AssassinsTheme.min.css" />
        
        <!-- jQuery Mobile Setup -->
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0/jquery.mobile.structure-1.0.min.css" />
		<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
	    <script src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script>
		
	</head> 

	
	<body>
	<div data-role="page" data-theme="a">
		<div data-role="header">
            <!-- link to home page, only show if player is logged in -->
            <? if ($methodName != "login" && $methodName != "register" && $methodName != "instructions"): ?>
		        <a href="<?= base_url('user/home') ?>" data-role="button" data-icon="home" data-iconpos="notext">Home</a>
		    <? endif ?>
			<h1><?= htmlspecialchars($title) ?></h1>	
		</div><!-- header -->
		
		<div data-role="content" data-theme="a">
