﻿<?
/*****************************************************************************
 * user.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Controller file. Calls game_model.php and user_model.php models.
 * Includes methods for handling website after database has been loaded.
 * Methods generally get data from model, ready it for consumption by the
 * view, and then pass it to the view via the renderPage private method.
 ****************************************************************************/

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("game_model");
    }
    
    /*
	 * Render the appropriate view
	 *
	 * @access	private
	 * @param	array
	 * @param	array
	 * @return	void
	 */
    private function renderPage($meta, $data) {
        // this method is responsible for ultimately rendering views

        // render page if user is logged in or trying to log in or register
        if($this->session->userdata("logged_in") || $meta["methodName"] == "login" || $meta["methodName"] == "register") {
            $this->load->view("templates/header", $meta);
            $this->load->view("assassins/" . $meta["methodName"], $data);
        }
               
        // otherwise redirect to login page
        else {
            $meta["title"] = "Assassins Mobile";
            $meta["methodName"] = "login";
            $meta["mode"] = "none";
            $data["errorMessage"] = "You must log in first!";
            $this->load->view("templates/header", $meta);
            $this->load->view("assassins/login", $data);
        }
            
        $this->load->view("templates/footer", $meta);           
    }

    /*
	 * If form not submitted, displays form for editing one's profile.
	 * Otherwise shows error message or updates the information and redirects to profile page.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function editProfile() {
        $meta["title"] = "Edit Profile";
        $meta["methodName"] = "editProfile";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";
        
        // fetch profile information to prepopulate form fields
        $data["playerData"] = $this->user_model->getProfile($this->session->userdata("id"));
        
        // check if form submitted
        if ($this->input->post("submit")) {
        	// try updating profile
            $error = $this->user_model->updateProfile($this->input->post());
            
            // if no error redirect to profile, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("user/profile"));
            else 
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }

    /*
	 * Display summary of past games played and administered.
	 *
	 * @access	public
	 * @return	void
	 */       
    public function history() {
        $meta["title"] = "Game History";
        $meta["methodName"] = "history";
        $meta["mode"] = "none";
        
        // fetch data about past played games
        $data["playedGames"] = $this->user_model->getGames(true, true);
        
        // fetch data about past administerd games
        $data["adminedGames"] = $this->user_model->getGames(false, true);
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * Fetches current games of user and displays home page.
	 *
	 * @access	public
	 * @return	void
	 */
    public function home() {  
    	// remove user from whatever game is currently open
        $unsetItems = array("game" => "", "gameName" => "", "mode" => "");
        $this->session->unset_userdata($unsetItems);
    
        $meta["title"] = "Home";
        $meta["methodName"] = "home";
        $meta["mode"] = "none";
        
        
        // fetch info about games user is playing
        $data["playerGames"] = $this->user_model->getGames(true, false);
        
        // fetch info about games user is administering
        $data["adminGames"] = $this->user_model->getGames(false, false);
        
        $this->renderPage($meta, $data);
    }

    /*
	 * Index redirects to home page
	 *
	 * @access	public
	 * @return	void
	 */       
    public function index() {
        header("Location: " . base_url("user/login"));
    }
    
    /*
	 * If form not submitted, displays form for joining a game.
	 * Otherwise shows error message or adds user to game and redirects to home page.
	 *
	 * @access	public
	 * @return	void
	 */
    public function join() {
        $meta["title"] = "Join Game";
        $meta["methodName"] = "join";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";
        
        // check if form submitted
        if ($this->input->post("submit")) {
            // try to add player to game
            $error = $this->game_model->addPlayer($this->input->post("gameName"),$this->input->post("gamePassword"));
            
            // if no error redirect to home page, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("user/home"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * If form not submitted, displays login form.
	 * Otherwise displays error message or logs in user and redirects to home page.
	 *
	 * @access	public
	 * @return	void
	 */
    public function login() {
        if ($this->session->userdata("id") != NULL)
            header("Location: " . base_url("user/home"));
    
        $meta["title"] = "Assassins Mobile";
        $meta["methodName"] = "login";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";    
    
        // check if form submitted already
        if ($this->input->post("submit")) {
            // try logging in user
            $error = $this->user_model->login($this->input->post("email"), $this->input->post("password"));
        
            // if successful, redirect to home page, otherwise display error message
            if($error === "no error")
                header("Location: " . base_url("user/home"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * Logs out user and redirects to login page.
	 *
	 * @access	public
	 * @return	void
	 */
    public function logout() {
        // destroy current session
        $this->session->sess_destroy();
        
        // redirect to login page
        header("Location: " . base_url("user/login"));
    }

    /*
	 * Displays user's profile information
	 *
	 * @access	public
	 * @return	void
	 */   
    public function profile() {
        $meta["title"] = "Profile";
        $meta["methodName"] = "profile";
        $meta["mode"] = "none"; 
        
        // fetch information about the player
        $data["playerData"] = $this->user_model->getProfile($this->session->userdata("id"));
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * If form not submitted, displays register form.
	 * Otherwise sends error message or registers user and redirects to home.
	 *
	 * @access	public
	 * @return	void
	 */
    public function register() {
        $meta["title"] = "Register";
        $meta["methodName"] = "register";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";
    
        // check if form submitted already
        if ($this->input->post("submit")) {
            // check if passwords match
            if ($this->input->post("password") != $this->input->post("password2")) {
                $data["errorMessage"] = "Passwords do not match";
                continue;
            }
            
            // create array of user input (everything except for 2nd password)
            $params = array ("name" => $this->input->post("name"),
                             "email" => $this->input->post("email"),
                             "password" => $this->input->post("password"),
                             "desc" => $this->input->post("description"),
                             "loc" => $this->input->post("location") );
            
            // try to insert user into database and log user in                 
            $error = $this->user_model->create($params);
            
            // if successful, redirect to homepage, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("user/home"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data); 
               
    }

    /*
	 * Displays information about user's target.
	 *
	 * @access	public
	 * @return	void
	 */       
    public function target() {
        $meta["title"] = $this->session->userdata("gameName");
        $meta["methodName"] = "target";
        $meta["mode"] = $this->session->userdata("mode");
        
        // fetch profile information about target
        $data["targetData"] = $this->user_model->getTarget();
        
        // fetch game's status (before, during, or after)
        $data["gameStatus"] = $this->game_model->gameStatus();
        
        $this->renderPage($meta, $data);
    }
}

?>