<?
/*****************************************************************************
 * game.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 1
 *
 * Controller file. Calls game_model.php and user_model.php models.
 * Includes methods for handling website after database has been loaded.
 * Methods generally get data from model, ready it for consumption by the
 * view, and then pass it to the view via the renderPage private method.
 ****************************************************************************/
 
class Game extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("game_model");
    }
    
    /*
	 * Render the appropriate view
	 *
	 * @access	private
	 * @param	array
	 * @param	array
	 * @return	void
	 */
    private function renderPage($meta, $data) {
        // this method is responsible for ultimately rendering views
        if ($meta["mode"] != "none")
            $meta["title"] = $this->session->userdata("gameName");
        
        // render page if user is logged in or trying to log in, register, or view the site info
        if($this->session->userdata("logged_in") 
           || $meta["methodName"] == "login" 
           || $meta["methodName"] == "register" 
           || $meta["methodName"] == "instructions") {
            $this->load->view("templates/header", $meta);
            $this->load->view("assassins/" . $meta["methodName"], $data);
        }
                  
        // otherwise redirect to login page
        else {
            $meta["title"] = "Assassins Mobile";
            $meta["methodName"] = "login";
            $meta["mode"] = "none";
            $data["errorMessage"] = "You must log in first!";
            $this->load->view("templates/header", $meta);
            $this->load->view("assassins/login", $data);
        }
            
        $this->load->view("templates/footer", $meta);           
    }
    
    /*
	 * If form not submitted, displays create form.
	 * Otherwise sends error message or creates game and redirects to game rules.
	 *
	 * @access	public
	 * @return	void
	 */
    public function create() {
        $meta["title"] = "Create Game";
        $meta["methodName"] = "create";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";
        
        // check if form submitted
        if($this->input->post("submit")) {
            // try to create new game
            $error = $this->game_model->create($this->input->post());
            
            // redirect to rules if no error, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("game/rules"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);   
    }

    /*
	 * If form not submitted, displays form to edit rules.
	 * Otherwise sends error message or updates game rules.
	 *
	 * @access	public
	 * @return	void
	 */
    public function editRules() {
        $meta["title"] = "Edit Rules";
        $meta["methodName"] = "editRules";
        $meta["mode"] = "none";
        
        $data["errorMessage"] = "";
        $data["rules"] = $this->game_model->getRules();
        
        // check if form submitted
        if ($this->input->post("submit")) {
            $error = $this->game_model->updateRules($this->input->post());
            
            // redirects to rules if no error, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("game/rules"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }

    /*
	 * Index redirects to login page
	 *
	 * @access	public
	 * @return	void
	 */    
    public function index() {
        header("Location: " . base_url("user/login"));
    }

    /*
	 * Displays site instructions.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function instructions() {
        $meta["title"] = "Website Instructions";
        $meta["methodName"] = "instructions";
        $meta["mode"] = "none";
        
        $this->renderPage($meta, NULL);
    }

    /*
	 * Displays kills made during a game by a single player or by all players
	 *
	 * @access	public
	 * @return	void
	 */    
    public function kills() {
        $meta["methodName"] = "kills";
        $meta["mode"] = $this->session->userdata("mode");

        $data["kills"] = $this->game_model->getKills();
        
        $this->renderPage($meta, $data);
    }

    /*
	 * If form not submitted, displays form to log kills.
	 * Otherwise sends error message or records the kill.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function logKill() {
        $meta["methodName"] = "kill";
        $meta["mode"] = $this->session->userdata("mode");
        
        $data["errorMessage"] = "";
        
        // check if form submitted
        if ($this->input->post("submit")) {
            $error = $this->game_model->addKill($this->input->post("killPassword"));
            
            // if successful record kill and inform player, otherwise send error message
            if ($error === "no error")
                $data["errorMessage"] = $this->game_model->killSuccess();
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }

    /*
	 * Displays notifications relevant to a user.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function notifications() {
        $meta["methodName"] = "notifications";
        $meta["mode"] = $this->session->userdata("mode");
        
        $data["notifications"] = $this->game_model->getNotifications();
        
        $this->renderPage($meta, $data);
    }

    /*
	 * If form not submitted, displays notification form.
	 * Otherwise sends error message or sends out notification.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function notify() {
        $meta["methodName"] = "notify";
        $meta["mode"] = $this->session->userdata("mode");
        
        $data["errorMessage"] = "";
        
        // check if form submitted
        if ($this->input->post("submit")) {
        	// try sending out the notification
            $error = $this->game_model->adminNotification($this->input->post("content"));
            
            // if successful redirect to notifications page, otherwise display error message
            if ($error === "no error")
                header("Location: " . base_url("game/notifications"));
            else
                $data["errorMessage"] = $error;
        }
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * Opens one of the current user's games and redirects to rules page.
	 *
	 * @access	private
	 * @return	void
	 */
    public function open($gameID) {
        // try to open game
        if ($this->game_model->open($gameID))
            header("Location: " . base_url("game/rules"));
        // redirect to home page if unsuccessful
        // users shouldn't be able to try to open a game they're not registered in unless the directly type in the url
        //      but open method in model will check to make sure current user is actually registered in the game
        else
            header("Location: " . base_url("user/home"));
    }
    
    /*
	 * Display game players and their targets (or if they have been eliminated or have won)
	 *
	 * @access	public
	 * @return	void
	 */    
    public function players() {
        $meta["methodName"] = "players";
        $meta["mode"] = $this->session->userdata("mode");
        
        $data["players"] = $this->game_model->getPlayers();
        
        $data["gameStatus"] = $this->game_model->gameStatus();
        
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * Retrieves rules of current game and displays them.
	 *
	 * @access	private
	 * @return	void
	 */
    public function rules() {
        $meta["methodName"] = "rules";
        
        // check whether user is playing or administering the current game
        $meta["mode"] = $this->session->userdata("mode");
        $data["mode"] = $meta["mode"];
        
        // fetch the rules of the game
        $data["game"] = $this->game_model->getRules();
        
        // get the kill password if player
        if ($meta["mode"] == "player")
        	$data["killPassword"] = $this->game_model->getPassword();
        
        $this->renderPage($meta, $data);
    }
    
    /*
	 * Start a game and redirect to notifications page.
	 * If unsuccessful, game will not start but notifications page will have a notification explaining why.
	 *
	 * @access	public
	 * @return	void
	 */    
    public function startGame() {
        $this->game_model->start();
        header("Location: " . base_url("game/notifications"));
    }
}

?>
