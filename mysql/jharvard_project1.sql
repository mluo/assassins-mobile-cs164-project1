-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2012 at 10:07 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jharvard_project1`
--

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
CREATE TABLE IF NOT EXISTS `games` (
  `gameID` int(11) NOT NULL AUTO_INCREMENT,
  `adminID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `killRules` mediumtext NOT NULL,
  `safetyRules` mediumtext NOT NULL,
  `moreRules` mediumtext NOT NULL,
  PRIMARY KEY (`gameID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`gameID`, `adminID`, `name`, `password`, `startDate`, `endDate`, `killRules`, `safetyRules`, `moreRules`) VALUES
(1, 1, 'Game 1', 'cs164', '2012-03-22 15:11:37', '2012-03-22 15:16:30', 'Touch with spoon or fork.', 'No killing in dining halls.', 'None'),
(2, 4, 'Game 2', 'cs164', NULL, NULL, 'Wink.', 'No safety zones.', 'Winner gets pizza!');

-- --------------------------------------------------------

--
-- Table structure for table `kills`
--

DROP TABLE IF EXISTS `kills`;
CREATE TABLE IF NOT EXISTS `kills` (
  `gameID` int(11) NOT NULL,
  `killerID` int(11) NOT NULL,
  `victimID` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `killID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`killID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kills`
--

INSERT INTO `kills` (`gameID`, `killerID`, `victimID`, `time`, `killID`) VALUES
(1, 2, 3, '2012-03-22 15:14:35', 1),
(1, 4, 2, '2012-03-22 15:16:30', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `notificationID` int(11) NOT NULL AUTO_INCREMENT,
  `gameID` int(11) NOT NULL,
  `playerID` int(11) NOT NULL,
  `content` mediumtext NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`notificationID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notificationID`, `gameID`, `playerID`, `content`, `time`) VALUES
(1, 1, 1, 'The game failed to start because the number of players is 0.', '2012-03-22 15:05:15'),
(2, 1, 1, 'Player 1 has joined the game.', '2012-03-22 15:06:16'),
(3, 1, 1, 'Player 2 has joined the game.', '2012-03-22 15:08:29'),
(4, 2, 0, 'Test notification.', '2012-03-22 15:10:20'),
(5, 1, 1, 'Player 3 has joined the game.', '2012-03-22 15:10:36'),
(6, 1, 0, 'Hey, everyone!', '2012-03-22 15:11:28'),
(7, 1, 0, 'The game has started with 3 players.', '2012-03-22 15:11:37'),
(8, 1, 0, 'The rules of this game have been updated by the administrator.', '2012-03-22 15:11:54'),
(9, 1, 1, 'Player 1 has killed Player 2. There are now 2 players left in the game.', '2012-03-22 15:14:35'),
(10, 1, 2, 'You have killed Player 2.', '2012-03-22 15:14:35'),
(11, 1, 3, 'You were killed by Player 1. You placed number 3 in this game.', '2012-03-22 15:14:35'),
(12, 1, 1, 'Player 3 has killed Player 1. There are now 1 players left in the game.', '2012-03-22 15:16:30'),
(13, 1, 4, 'You have killed Player 1.', '2012-03-22 15:16:30'),
(14, 1, 2, 'You were killed by Player 3. You placed number 2 in this game.', '2012-03-22 15:16:30'),
(15, 1, 1, 'Player 3 has won the game.', '2012-03-22 15:16:30'),
(16, 1, 4, 'Congratulations, you are the winner!', '2012-03-22 15:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE IF NOT EXISTS `players` (
  `playerID` int(11) NOT NULL,
  `gameID` int(11) NOT NULL,
  `targetID` int(11) DEFAULT NULL,
  `killPassword` varchar(255) NOT NULL,
  `place` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`playerID`, `gameID`, `targetID`, `killPassword`, `place`) VALUES
(2, 1, NULL, 'wrath', 2),
(3, 1, NULL, 'shirt', 3),
(4, 1, 4, 'magikarp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `desc` mediumtext NOT NULL,
  `loc` varchar(255) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `name`, `email`, `password`, `desc`, `loc`) VALUES
(1, 'Admin', 'admin@test.com', '94f9eeb4ff360e782ecadcd67d7df915', 'Tall, handsome, Asian, bulging biceps.', 'Eliot House'),
(2, 'Player 1', 'player1@test.com', '94f9eeb4ff360e782ecadcd67d7df915', 'Short, skinny, glasses, spiky hair.', 'Kirkland House'),
(3, 'Player 2', 'player2@test.com', '94f9eeb4ff360e782ecadcd67d7df915', 'Blonde, nice butt, wears high heels all the time, likes plaid.', 'Winthrop House'),
(4, 'Player 3', 'player3@test.com', '94f9eeb4ff360e782ecadcd67d7df915', 'Athletic girl who loves wearing tank tops.', 'Cabot House.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
