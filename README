﻿# CS 164 - PROJECT 1 - ASSASSINS MOBILE - MICHELLE LUO AND EVAN WU
# RELEASE VERSION
#
# Instructions on setting up the website are at the bottom of this README.
# 
# Please click on the "Website Instructions" link on the login page to read about
# how the website works and how it facilitates the game of Assassins. 
# 
# To fully test all the functionalities of this website in real time, you will 
# need at least three people. 
#
# To facilitate testing alone, we have created 4 test accounts for you:
# admin@test.com, player1@test.com, player2@test.com, and player3@test.com.
# The password for each is "cs164." With these accounts, we have already set
# up and completed one game called "Game 1". In this game, admin@test.com served 
# as the creator and administrator, and player3@test.com ultimately won. To access it, 
# simply log in as any of the players, and find the link to the game from the Game History
# page. You will also notice that player3@test.com has created another game called
# "Game 2," but no one has joined yet. You have the option of joining that game, which has
# password "cs164," with the other three accounts and playing it out. Of course,
# we also encourage you to register your own account and create your own game
# in order to fully experience the website. 
#
# POSSIBLE FUTURE IMPROVEMENTS OF DESIGN
# * It would be nice to have a way for users to recover forgotten passwords.
# * It would better facilitate users if the website emailed notifications.
# * It would be nice if the notifications page highligted the notifications
#   that the user has not seen before, like Facebook does.
# * When someone wins, the notifications page puts the "You have killed..." notification
#   on top of the "Congratulations..." notification. The order should ideally be
#   reversed, but it is in the current order because the two notifications happen at the
#   same second. We tried reversing the order by using PHP sleep(), but it doesn't seem to be working.
# * It would be nice to have a way for users to edit their passwords, with confirmation of 
#   old password necessary for inputting a new one.
# * Right now, all game names must be unique. It would be nice if a new game could have the
#   same name as an already-existing game, if and only if the previous game had already
#   finished.
# * On the back end, there is a little bit of redundancy between the game and user models,
#   as well as between the game and user controllers. It might be cleaner to restructure
#   the models and the controllers so that there is a parent model and parent controller
#   that factor out these redundancies.
# * We are a little concerned that there are so many database queries that need to be 
#   excecuted in order for us to achieve our desired functionality. To optimize the site,
#   we could reexamine the logic of our models and databases and consider whether there might
#   be ways to reduce the number of separate database queries or otherwise optimize them.

# HOW TO SET UP THE WEBSITE

# clone repo into ~/vhosts/mluo
cd ~/vhosts
git clone git@bitbucket.org:mluo/project1.git mluo

# Change CSS href link
# Go to header.php in ~/vhosts/mluo/application/views/templates/ and
# change line 21 to "<link rel="stylesheet" href="http://mluo/themes/AssassinsTheme.min.css" />"
gedit ~/vhosts/mluo/application/views/templates/header.php

# go to config.php in ~/vhosts/mluo/application/config/ folder and change the base_url to http://mluo/index.php/
gedit ~/vhosts/mluo/application/config/config.php

# chmod all directories 711
find ~/vhosts/mluo -type d -exec chmod 711 {} \;

# chmod all PHP files 600
find ~/vhosts/mluo -type f -name *.php -exec chmod 600 {} \;

# chmod most everything else 644
find ~/vhosts/mluo -type f \( -name *.css -o -name *.gif -o -name *.html -o -name *.js -o -name *.jpg -o -name *.png -o -name .htaccess \) -exec chmod 644 {} \;

# create a MySQL database for project
mysql -u jharvard -p -e 'CREATE DATABASE jharvard_mluo'

# import SQL dump into database 
mysql -u jharvard -p jharvard_mluo < ~/vhosts/mluo/mysql/jharvard_project1.sql

# change the value of $db['default']['database'] to be 'jharvard_mluo'
gedit ~/vhosts/mluo/application/config/database.php

# append '127.0.0.1 mluo' to /etc/hosts
set -o noclobber
sudo gedit /etc/hosts

# go to http://mluo/index.php/user/login to access the website!
